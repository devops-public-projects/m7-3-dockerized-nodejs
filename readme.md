
# Dockerizing a NodeJS app and Storing it in an ECR Private Docker Repository 
This project will involve Dockerizing a Nodejs application and pushing it to a private Docker registry. We will be simulating what Jenkins would do after we commit code to a Git repository and the CI/CD pipeline actions ared triggered.

## Technologies Used
- Docker
- Amazon ECR
- Node.js
- Git
- Linux (Ubuntu)

## Project Description
- Write a Dockerfile to build a Docker image for a Node.js application 
- Create a private Docker registry on AWS (Amazon ECR)
- Push Docker image to this private repository

## Prerequisites
- A Linux machine with Docker, Git, Node.js and NPM installed
- An AWS account
- [AWS CLI Installed](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- [AWS CLI Configured](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html)

## Guide Steps
### Build The Docker Image
We'll use our Dockerfile which is configured to use node, basic environment variables and an entry point.
- Navigate to the project folder
	- `cd /project_folder/app`
- Build the Docker Image
	- `docker build -t my-app:1.0 .`
- To run the application
	- `docker run my-app:1.0`

![Docker Image Built](/images/m7-3-docker-image-built.png)

### Create Private Docker Repository in Amazon ECR
- Log into your AWS account and look for Elastic Container Registry (ECR) and Get Started/Create a new Repository
	- We'll set the name of the repository to `my-app` for this example and it will be a `private` repo.
- Open the repository and `View push commands`
	- Utilize the first command to Docker login for AWS. The command will look like this:
		- `aws ecr get-login-password --region YOUR_LOCATION | docker login --username AWS --password-stdin YOUR_REPOSITORY_.amazonaws.com`
		- This command will report `login succeeded`  for a success.

### Upload Docker Image to Private Docker Repository
- Tag Our Image
	- `docker tag my-app:1.0 YOUR_REPOSITORY.amazonaws.com/my-app:1.0`
	- If you run `Docker images` from your CLI you will see a new entry for the renamed app.
- Push our Image
	- `docker push YOUR_REPOSITORY.amazonaws.com/my-app:1.0`

![Docker Image Pushed Successfully](/images/m7-3-docker-image-pushed.png)


If you want to push a new version you will need to run `Docker build`, `Docker tag` and `Docker push` commands again with the new `app:tag`.

![Second Docker Image Pushed Successfully](/images/m7-3-second-image-pushed.png)